﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Divisible.aspx.cs" Inherits="Bonus_Assignment.Divisible" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label Text="Type a number" runat="server" />
            <asp:TextBox runat="server" ID="num"/>
            <div id="result" runat="server"></div>
            <div id="message" runat="server"></div>
            <asp:Button Text="Submit" ID="btnsubmit" runat="server" onClick="DCheck"/>
        </div>
    </form>
</body>
</html>
