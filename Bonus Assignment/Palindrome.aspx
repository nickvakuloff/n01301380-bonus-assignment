﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="Bonus_Assignment.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label Text="Type your word" runat="server" />
            <br />
            <asp:TextBox ID="word" runat="server" />         
            <div id="result" runat="server"></div>
            <asp:Button Text="Submit" ID="btnsubmit" runat="server" onClick="WordCheck"/>
        </div>
    </form>
</body>
</html>
