﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Axis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void AxisCheck(object sender, EventArgs e)
        {
            int x = int.Parse(axisX.Text);
            int y = int.Parse(axisY.Text);
            if (x > 0)
            {
                if (y > 0)
                {
                    result.InnerHtml = "Quadrant 1";
                }
                else
                {
                    result.InnerHtml = "Quadrant 4";
                }
            }
            else
            {
                if (y > 0){
                    result.InnerHtml = "Quadrant 2";
                }
                else
                {
                    result.InnerHtml = "Quadrant 3";
                }
            }
        }

    }
}