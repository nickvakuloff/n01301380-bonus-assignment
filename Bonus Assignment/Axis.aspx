﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Axis.aspx.cs" Inherits="Bonus_Assignment.Axis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label Text="Type X" runat="server" />
            <br />
            <asp:TextBox ID="axisX" runat="server" />         
            <asp:CompareValidator ErrorMessage="X must be not equal to 0" ControlToValidate="axisX" Type="Integer" ValueToCompare="0" Operator="NotEqual" runat="server" />
            <br />
            <asp:Label Text="Type Y" runat="server" />
            <br />
            <asp:TextBox ID="axisY" runat="server" />         
            <asp:CompareValidator ErrorMessage="Y must be not equal to 0" ControlToValidate="axisY" Type="Integer" ValueToCompare="0" Operator="NotEqual" runat="server" />
            <br />
            <div id="result" runat="server"></div>
            <asp:Button Text="Submit" ID="btnsubmit" runat="server" onClick="AxisCheck"/>
        </div>
    </form>
</body>
</html>
