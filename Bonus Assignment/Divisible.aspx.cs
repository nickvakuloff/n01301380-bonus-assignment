﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Divisible : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void DCheck(object sender, EventArgs e)
        {
            int x = int.Parse(num.Text);
            int k = 0;
            for (int i=x; i>0; i--)
            {
                int a = x % i;
                if (a == 0)
                { 
                    k++;
                    if(k>2)
                    {
                        message.InnerHtml = "Not prime";
                    }
                }
                else
                {
                    message.InnerHtml = "Prime";
                }
            }
        }
    }
}